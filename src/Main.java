

import com.z4zr.java.joker.common.Command;
import com.z4zr.java.joker.server.Server;
import com.z4zr.java.joker.service.DemoService;

public class Main {
	public static void main(String[] args) {
		Server s = new Server();
		Thread t = new Thread(s);
		t.start();
		
		s.addService(new DemoService("aaaa", 10));
		s.addCommand(new Command("aaaa", "RUN"));
		s.addService(new DemoService("bbbb", 6));
		s.addCommand(new Command("bbbb", "RUN"));
		s.addService(new DemoService("cccc", 15));
		s.addCommand(new Command("cccc", "RUN"));
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		s.removeService("cccc");
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		s.shutdown();
	}
}