package com.z4zr.java.joker.server;

import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.z4zr.java.joker.common.Command;
import com.z4zr.java.joker.service.ServiceImpl;
import com.z4zr.java.joker.service.ServiceComparator;
import com.z4zr.java.joker.service.ServiceStatus;

/**
 * 适用于命令的添加速度和频率较低的场景
 */

public class Server implements Runnable {
	protected static Logger log = LogManager.getLogger(Server.class);
	private TreeSet<ServiceImpl> services;// 服务集合树
	private ReadWriteLock rwl = new ReentrantReadWriteLock();
	private boolean live;
	/**
	 * 指令队列不会很大，不会出现密集的指令执行，所以暂定未成功注册的指令会回到指令队列尾部等待轮回。。
	 */
	private ConcurrentLinkedQueue<Command> cmdQueue;

	public Server() {
		log.info("joker后台服务管理初始化");
		this.services = new TreeSet<ServiceImpl>(new ServiceComparator());
		this.cmdQueue = new ConcurrentLinkedQueue<Command>();
		live = true;
	}

	@Override
	public void run() {
		routine();
	}

	/**
	 * 由于执行过程中会消耗一定的时间，故不可通过此方法的延时作为时钟机制
	 */
	public void routine() {
		while (live) {
			// System.out.println(cmdQueue.isEmpty());
			if (cmdQueue.isEmpty()) {
				rwl.readLock().lock();
				try {
					for (ServiceImpl serv : services) {
						serv.routine();// 无耗时的小行为
					}
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					rwl.readLock().unlock();
				}
			} else {
				Command cmd = cmdQueue.poll();
				ServiceImpl serv = getService(cmd.name);
				if (!serv.addCommand(cmd)) {
					cmdQueue.add(cmd);
				} else {
					serv.routine();// 无耗时的小行为（执行添加的命令）
				}
			}
		}
		doShutdown();
	}

	/**
	 * 终止服务管理前清理在运行服务
	 */
	private void doShutdown() {
		// 服务管理终止 所以服务的状态均为stop 才可出去
		try {
			rwl.writeLock().lock();
			Command c = new Command(null, "STOP");
			for (ServiceImpl serv : services.descendingSet()) {
				log.info("joker向 [" + serv.getName() + "] 添加终止指令");
				serv.addCommand(c);
				serv.routine();
			}
			Thread.sleep(1000);
			for (ServiceImpl serv : services) {
				log.info("joker终止服务 [" + serv.getName() + "] " + (serv.getStatus().equals(ServiceStatus.STOP) ? "SUCCESS" : "FAILE"));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			rwl.writeLock().unlock();
		}
		log.info("joker后台服务管理终止");
	}

	/**
	 * 添加指令
	 * @param cmd
	 * @return
	 */
	public synchronized boolean addCommand(Command cmd) {
		return cmdQueue.add(cmd);
	}

	/**
	 * 添加service
	 * @param service
	 * @return
	 */
	public synchronized boolean addService(ServiceImpl service) {
		try {
			rwl.writeLock().lock();
			return services.add(service);
		} finally {
			rwl.writeLock().unlock();
		}
	}

	/**
	 * 移除服务
	 * @param name
	 * @return 移除服务成功或服务不存在返回true
	 */
	public synchronized boolean removeService(String name) {
		ServiceImpl serv = getService(name);
		if (serv != null) {
			serv.addCommand(new Command(null, "STOP"));
			serv.routine();
			if(serv.getStatus().equals(ServiceStatus.STOP)){//
				try {
					rwl.writeLock().lock();
					return services.remove(serv);
				} finally {
					rwl.writeLock().unlock();
				}
			}else{
				log.error("joker移除服务 [" + name + "] 时未成功停止");
				return false;
			}
		} else {
			log.info("joker移除服务 [" + name + "] 不存在");
			return true;
		}
	}

	/**
	 * 获取当前全部线程状态的预留方法
	 */
	public synchronized void displayStatus() {
		// TODO 折腾出来所有服务的状态
	}

	/**
	 * 使用名称获取指定service
	 * @param name
	 * @return
	 */
	public ServiceImpl getService(String name) {
		try {
			rwl.readLock().lock();
			for (ServiceImpl serv : services) {
				if (serv.is(name)) {
					return serv;
				}
			}
		} finally {
			rwl.readLock().unlock();
		}
		return null;
	}

	/**
	 * 终止框架的方法
	 */
	public void shutdown() {
		live = false;
	}

}
