package com.z4zr.java.joker.common;

public class Command {
	public String name;
	public String cmd;
	
	public Command(String name,String cmd) {
		this.name = name;
		this.cmd = cmd;
	}
}
