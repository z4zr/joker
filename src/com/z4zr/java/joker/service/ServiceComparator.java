package com.z4zr.java.joker.service;

import java.util.Comparator;

public class ServiceComparator implements Comparator<ServiceImpl> {

	@Override
	public int compare(ServiceImpl o1, ServiceImpl o2) {
		return o1.getPriviliege()-o2.getPriviliege();
	}
}
