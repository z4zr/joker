package com.z4zr.java.joker.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.z4zr.java.joker.common.Command;

public abstract class ServiceImpl implements Service {
	protected static Logger log = LogManager.getLogger(ServiceImpl.class);
	private String name;
	private int priviliege;
	private boolean haveCommand;
	private Command command;
	private ServiceStatus status;

	public ServiceImpl(String name, int priviliege) {
		this.name = name;
		this.priviliege = priviliege;
		this.haveCommand = false;
		init();
		status = ServiceStatus.READY;
		log.info("初始化服务 [" + this.name + "] DONE");
	}

	@Override
	public final boolean is(String name) {
		return this.name.equals(name) ? true : false;
	}

	@Override
	public final boolean addCommand(Command cmd) {
		if (haveCommand) {
			return false;
		} else {
			haveCommand = true;
			command = cmd;
			return true;
		}
	}

	@Override
	public final void routine() {
		if (haveCommand) {
			if (command.cmd.equals("RUN") && status.equals(ServiceStatus.READY)) {// READY状态下可用.
				this.status = ServiceStatus.RUNNING;
			} else if (this.command.cmd.equals("HANGUP") && status.equals(ServiceStatus.RUNNING)) {// RUN状态下可用
				hangup();
				status = ServiceStatus.READY;
			} else if (this.command.cmd.equals("STOP") && (status.equals(ServiceStatus.RUNNING) || status.equals(ServiceStatus.READY))) {// RUN、READY状态下可用
				log.info("服务 [" + this.name + "] 执行终止流程");
				shutdown();
				status = ServiceStatus.STOP;
			} else if (this.command.cmd.equals("INIT") && status.equals(ServiceStatus.STOP)) {// STOP状态下可用
				init();
				this.status = ServiceStatus.READY;
			}
			haveCommand = false;
		} else if (status.equals(ServiceStatus.RUNNING)) {
			running();
		}
	}

	/**
	 * 创建线程并执行/初始化任务
	 */
	public abstract void init();

	/**
	 * 暂停线程/暂停任务
	 */
	public abstract void hangup();

	/**
	 * 终止线程/终止任务
	 */
	public abstract void shutdown();

	/**
	 * 线程状态检查/执行非耗时任务
	 */
	public abstract void running();

	public final String getName() {
		return name;
	}

	public final int getPriviliege() {
		return priviliege;
	}

	public final ServiceStatus getStatus() {
		return status;
	}

}
