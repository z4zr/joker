package com.z4zr.java.joker.service;

import com.z4zr.java.joker.common.Command;

public interface Service {
	/**
	 * 用于判断使用当前接口的类是否为所需的类
	 */
	public boolean is(String name);

	/**
	 * 例行任务接口,执行非耗时的running方法
	 */
	public void routine();
	
	/**
	 * 口令添加接口
	 */
	public boolean addCommand(Command cmd);
	
}
