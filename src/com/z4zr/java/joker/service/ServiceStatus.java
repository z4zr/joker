package com.z4zr.java.joker.service;

public enum ServiceStatus {
	/**
	 * 去执行初始化并进入READY
	 */
	INIT,
	/**
	 * 可进入运行状态（由command 确定进入RUNNING）
	 */
	READY,
	/**
	 * 运行
	 */
	RUNNING,
	/**
	 * 执行挂起并进入READY
	 */
	HANGUP,
	/**
	 * 检测当前状态，如不是当前状态 释放资源
	 */
	STOP
}
